<?php
require_once("../../vendor/autoload.php");

if(empty($_POST['studentID'])){
    echo "I'm a Person!<br>";
    $obj = new \App\Person();

    $obj->setName($_POST["userName"]);
    $obj->setDateOfBirth($_POST["dateOfBirth"]);

    echo $obj->getName()."<br/>";
    echo $obj->getDateOfBirth()."<br/>";
} else{
    echo "I'm a student!<br/>";
    $obj = new \Tap\Student();

    $obj->setName($_POST["userName"]);
    $obj->setStudentID($_POST["studentID"]);
    $obj->setDateOfBirth($_POST["dateOfBirth"]);

    echo $obj->getName()."<br/>";
    echo $obj->getStudentID()."<br/>";
    echo $obj->getDateOfBirth()."<br/>";
}
?>